﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UI.UserControls;

namespace UI.Forms
{
    public partial class UIWindow : Form
    {
        private UIAccident a;
        private UIRiskArea ra;
        private UIAccidentQuery aq;

        public UIWindow()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            this.altaDeFactoresDeRiesgoToolStripMenuItem_Click(null,null);
        }

        private void LoadUserControl(UserControl uc)
        {
            this.panel.Controls.Clear();
            this.panel.Controls.Add(uc);
        }

        private void altaDeFactoresDeRiesgoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ra == null)
            {
                this.ra = new UIRiskArea(this);
            }
            this.LoadUserControl(ra);
        }

        private void altaDeAccidenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.a == null)
            {
                this.a = new UIAccident(this);
            }
            this.LoadUserControl(this.a);
        }

        private void consultaAccidenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.aq == null)
            {
                this.aq = new UIAccidentQuery();
            }
            this.LoadUserControl(this.aq);
        }

        public void RiskAreaCancel()
        {
            this.ra = null;
            this.altaDeFactoresDeRiesgoToolStripMenuItem_Click(null,null);
        }

        public void AccidentCancel()
        {
            this.a = null;
            this.altaDeAccidenteToolStripMenuItem_Click(null,null);
        }


        

    }
}
