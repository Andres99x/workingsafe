﻿namespace UI.Forms
{
    partial class UIWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.altaDeFactoresDeRiesgoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaDeAccidenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel = new System.Windows.Forms.Panel();
            this.consultaAccidenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altaDeFactoresDeRiesgoToolStripMenuItem,
            this.altaDeAccidenteToolStripMenuItem,
            this.consultaAccidenteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(559, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // altaDeFactoresDeRiesgoToolStripMenuItem
            // 
            this.altaDeFactoresDeRiesgoToolStripMenuItem.Name = "altaDeFactoresDeRiesgoToolStripMenuItem";
            this.altaDeFactoresDeRiesgoToolStripMenuItem.Size = new System.Drawing.Size(137, 20);
            this.altaDeFactoresDeRiesgoToolStripMenuItem.Text = "Alta de Área de Riesgo";
            this.altaDeFactoresDeRiesgoToolStripMenuItem.Click += new System.EventHandler(this.altaDeFactoresDeRiesgoToolStripMenuItem_Click);
            // 
            // altaDeAccidenteToolStripMenuItem
            // 
            this.altaDeAccidenteToolStripMenuItem.Name = "altaDeAccidenteToolStripMenuItem";
            this.altaDeAccidenteToolStripMenuItem.Size = new System.Drawing.Size(112, 20);
            this.altaDeAccidenteToolStripMenuItem.Text = "Alta de Accidente";
            this.altaDeAccidenteToolStripMenuItem.Click += new System.EventHandler(this.altaDeAccidenteToolStripMenuItem_Click);
            // 
            // panel
            // 
            this.panel.AutoSize = true;
            this.panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(0, 24);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(559, 359);
            this.panel.TabIndex = 1;
            // 
            // consultaAccidenteToolStripMenuItem
            // 
            this.consultaAccidenteToolStripMenuItem.Name = "consultaAccidenteToolStripMenuItem";
            this.consultaAccidenteToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.consultaAccidenteToolStripMenuItem.Text = "Consulta Accidente";
            this.consultaAccidenteToolStripMenuItem.Click += new System.EventHandler(this.consultaAccidenteToolStripMenuItem_Click);
            // 
            // UIWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(559, 383);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "UIWindow";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Window_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem altaDeFactoresDeRiesgoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaDeAccidenteToolStripMenuItem;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.ToolStripMenuItem consultaAccidenteToolStripMenuItem;
    }
}

