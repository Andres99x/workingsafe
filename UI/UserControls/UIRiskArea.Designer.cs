﻿namespace UI.UserControls
{
    partial class UIRiskArea
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.bPre = new System.Windows.Forms.Button();
            this.bGeo = new System.Windows.Forms.Button();
            this.nLevel = new System.Windows.Forms.NumericUpDown();
            this.bDeleteRiskFactor = new System.Windows.Forms.Button();
            this.tRiskFactor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tableRiskFactors = new System.Windows.Forms.DataGridView();
            this.tId = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.bAddRiskFactor = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableRiskFactors)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bCancel);
            this.groupBox1.Controls.Add(this.bSave);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.bPre);
            this.groupBox1.Controls.Add(this.bGeo);
            this.groupBox1.Controls.Add(this.nLevel);
            this.groupBox1.Controls.Add(this.bDeleteRiskFactor);
            this.groupBox1.Controls.Add(this.tRiskFactor);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tableRiskFactors);
            this.groupBox1.Controls.Add(this.tId);
            this.groupBox1.Controls.Add(this.tName);
            this.groupBox1.Controls.Add(this.bAddRiskFactor);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 405);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alta de Área de Riesgo";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(316, 376);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 9;
            this.bCancel.Text = "Cancelar";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bSave
            // 
            this.bSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bSave.Location = new System.Drawing.Point(235, 376);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 8;
            this.bSave.Text = "Guardar";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 338);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Delimitación del Área:";
            // 
            // bPre
            // 
            this.bPre.Location = new System.Drawing.Point(270, 333);
            this.bPre.Name = "bPre";
            this.bPre.Size = new System.Drawing.Size(121, 23);
            this.bPre.TabIndex = 7;
            this.bPre.Text = "Área Predefinida";
            this.bPre.UseVisualStyleBackColor = true;
            this.bPre.Click += new System.EventHandler(this.bPre_Click);
            // 
            // bGeo
            // 
            this.bGeo.AutoSize = true;
            this.bGeo.Location = new System.Drawing.Point(127, 333);
            this.bGeo.Name = "bGeo";
            this.bGeo.Size = new System.Drawing.Size(121, 23);
            this.bGeo.TabIndex = 6;
            this.bGeo.Text = "Área Georeferenciada";
            this.bGeo.UseVisualStyleBackColor = true;
            this.bGeo.Click += new System.EventHandler(this.bGeo_Click);
            // 
            // nLevel
            // 
            this.nLevel.Location = new System.Drawing.Point(132, 301);
            this.nLevel.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nLevel.Name = "nLevel";
            this.nLevel.Size = new System.Drawing.Size(86, 20);
            this.nLevel.TabIndex = 5;
            // 
            // bDeleteRiskFactor
            // 
            this.bDeleteRiskFactor.AutoSize = true;
            this.bDeleteRiskFactor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bDeleteRiskFactor.Location = new System.Drawing.Point(338, 262);
            this.bDeleteRiskFactor.Name = "bDeleteRiskFactor";
            this.bDeleteRiskFactor.Size = new System.Drawing.Size(53, 23);
            this.bDeleteRiskFactor.TabIndex = 4;
            this.bDeleteRiskFactor.Text = "Eliminar";
            this.bDeleteRiskFactor.UseVisualStyleBackColor = true;
            this.bDeleteRiskFactor.Click += new System.EventHandler(this.bDeleteRiskFactor_Click);
            // 
            // tRiskFactor
            // 
            this.tRiskFactor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.tRiskFactor.Location = new System.Drawing.Point(115, 86);
            this.tRiskFactor.Name = "tRiskFactor";
            this.tRiskFactor.Size = new System.Drawing.Size(216, 20);
            this.tRiskFactor.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Nivel de Capacitación:";
            // 
            // tableRiskFactors
            // 
            this.tableRiskFactors.AllowUserToAddRows = false;
            this.tableRiskFactors.AllowUserToDeleteRows = false;
            this.tableRiskFactors.AllowUserToResizeRows = false;
            this.tableRiskFactors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tableRiskFactors.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableRiskFactors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableRiskFactors.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.tableRiskFactors.Location = new System.Drawing.Point(15, 112);
            this.tableRiskFactors.MultiSelect = false;
            this.tableRiskFactors.Name = "tableRiskFactors";
            this.tableRiskFactors.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.tableRiskFactors.RowHeadersVisible = false;
            this.tableRiskFactors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tableRiskFactors.ShowCellErrors = false;
            this.tableRiskFactors.ShowCellToolTips = false;
            this.tableRiskFactors.ShowEditingIcon = false;
            this.tableRiskFactors.ShowRowErrors = false;
            this.tableRiskFactors.Size = new System.Drawing.Size(376, 144);
            this.tableRiskFactors.TabIndex = 7;
            // 
            // tId
            // 
            this.tId.Location = new System.Drawing.Point(115, 34);
            this.tId.Name = "tId";
            this.tId.Size = new System.Drawing.Size(216, 20);
            this.tId.TabIndex = 0;
            this.tId.TextChanged += new System.EventHandler(this.tId_TextChanged);
            // 
            // tName
            // 
            this.tName.Location = new System.Drawing.Point(115, 60);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(216, 20);
            this.tName.TabIndex = 1;
            this.tName.TextChanged += new System.EventHandler(this.tName_TextChanged);
            // 
            // bAddRiskFactor
            // 
            this.bAddRiskFactor.AutoSize = true;
            this.bAddRiskFactor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bAddRiskFactor.Location = new System.Drawing.Point(337, 84);
            this.bAddRiskFactor.Name = "bAddRiskFactor";
            this.bAddRiskFactor.Size = new System.Drawing.Size(54, 23);
            this.bAddRiskFactor.TabIndex = 3;
            this.bAddRiskFactor.Text = "Agregar";
            this.bAddRiskFactor.UseVisualStyleBackColor = true;
            this.bAddRiskFactor.Click += new System.EventHandler(this.bAddRiskFactor_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Factore de Riesgo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(90, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Id:";
            // 
            // UIRiskArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.groupBox1);
            this.Name = "UIRiskArea";
            this.Size = new System.Drawing.Size(406, 411);
            this.Load += new System.EventHandler(this.UIRiskArea_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tableRiskFactors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bPre;
        private System.Windows.Forms.Button bGeo;
        private System.Windows.Forms.NumericUpDown nLevel;
        private System.Windows.Forms.Button bDeleteRiskFactor;
        private System.Windows.Forms.TextBox tRiskFactor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView tableRiskFactors;
        private System.Windows.Forms.TextBox tId;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.Button bAddRiskFactor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bCancel;
    }
}
