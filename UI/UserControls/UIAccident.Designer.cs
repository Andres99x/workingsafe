﻿namespace UI.UserControls
{
    partial class UIAccident
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.lCount = new System.Windows.Forms.Label();
            this.cbImpact = new System.Windows.Forms.ComboBox();
            this.bDeleteRiskFactor = new System.Windows.Forms.Button();
            this.tablePersonalImpact = new System.Windows.Forms.DataGridView();
            this.bAddRiskFactor = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.nInvolved = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.dDateTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.tRiskArea = new System.Windows.Forms.TextBox();
            this.tId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePersonalImpact)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInvolved)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bCancel);
            this.groupBox1.Controls.Add(this.bSave);
            this.groupBox1.Controls.Add(this.lCount);
            this.groupBox1.Controls.Add(this.cbImpact);
            this.groupBox1.Controls.Add(this.bDeleteRiskFactor);
            this.groupBox1.Controls.Add(this.tablePersonalImpact);
            this.groupBox1.Controls.Add(this.bAddRiskFactor);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nInvolved);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dDateTime);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tRiskArea);
            this.groupBox1.Controls.Add(this.tId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 382);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alta de Accidente";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(310, 353);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 22;
            this.bCancel.Text = "Cancelar";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bSave
            // 
            this.bSave.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bSave.Location = new System.Drawing.Point(229, 353);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 23;
            this.bSave.Text = "Guardar";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // lCount
            // 
            this.lCount.AutoSize = true;
            this.lCount.Location = new System.Drawing.Point(6, 313);
            this.lCount.Name = "lCount";
            this.lCount.Size = new System.Drawing.Size(24, 13);
            this.lCount.TabIndex = 21;
            this.lCount.Text = "0/0";
            // 
            // cbImpact
            // 
            this.cbImpact.FormattingEnabled = true;
            this.cbImpact.Items.AddRange(new object[] {
            "Fallecimiento",
            "Heridas Graves",
            "Heridas Leves - Con Hospitalización",
            "Heridas Leves - Sin Hospitalización",
            "Sin Heridas"});
            this.cbImpact.Location = new System.Drawing.Point(119, 135);
            this.cbImpact.Name = "cbImpact";
            this.cbImpact.Size = new System.Drawing.Size(206, 21);
            this.cbImpact.TabIndex = 20;
            // 
            // bDeleteRiskFactor
            // 
            this.bDeleteRiskFactor.AutoSize = true;
            this.bDeleteRiskFactor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bDeleteRiskFactor.Location = new System.Drawing.Point(332, 313);
            this.bDeleteRiskFactor.Name = "bDeleteRiskFactor";
            this.bDeleteRiskFactor.Size = new System.Drawing.Size(53, 23);
            this.bDeleteRiskFactor.TabIndex = 19;
            this.bDeleteRiskFactor.Text = "Eliminar";
            this.bDeleteRiskFactor.UseVisualStyleBackColor = true;
            this.bDeleteRiskFactor.Click += new System.EventHandler(this.bDeleteRiskFactor_Click);
            // 
            // tablePersonalImpact
            // 
            this.tablePersonalImpact.AllowUserToAddRows = false;
            this.tablePersonalImpact.AllowUserToDeleteRows = false;
            this.tablePersonalImpact.AllowUserToResizeRows = false;
            this.tablePersonalImpact.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.tablePersonalImpact.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tablePersonalImpact.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablePersonalImpact.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.tablePersonalImpact.Location = new System.Drawing.Point(9, 163);
            this.tablePersonalImpact.MultiSelect = false;
            this.tablePersonalImpact.Name = "tablePersonalImpact";
            this.tablePersonalImpact.ReadOnly = true;
            this.tablePersonalImpact.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.tablePersonalImpact.RowHeadersVisible = false;
            this.tablePersonalImpact.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tablePersonalImpact.ShowCellErrors = false;
            this.tablePersonalImpact.ShowCellToolTips = false;
            this.tablePersonalImpact.ShowEditingIcon = false;
            this.tablePersonalImpact.ShowRowErrors = false;
            this.tablePersonalImpact.Size = new System.Drawing.Size(376, 144);
            this.tablePersonalImpact.TabIndex = 18;
            // 
            // bAddRiskFactor
            // 
            this.bAddRiskFactor.AutoSize = true;
            this.bAddRiskFactor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bAddRiskFactor.Location = new System.Drawing.Point(331, 133);
            this.bAddRiskFactor.Name = "bAddRiskFactor";
            this.bAddRiskFactor.Size = new System.Drawing.Size(54, 23);
            this.bAddRiskFactor.TabIndex = 17;
            this.bAddRiskFactor.Text = "Agregar";
            this.bAddRiskFactor.UseVisualStyleBackColor = true;
            this.bAddRiskFactor.Click += new System.EventHandler(this.bAddRiskFactor_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Impacto en Personal:";
            // 
            // nInvolved
            // 
            this.nInvolved.Location = new System.Drawing.Point(119, 106);
            this.nInvolved.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nInvolved.Name = "nInvolved";
            this.nInvolved.Size = new System.Drawing.Size(120, 20);
            this.nInvolved.TabIndex = 1;
            this.nInvolved.ValueChanged += new System.EventHandler(this.nInvolved_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "N° de Involucrados:";
            // 
            // dDateTime
            // 
            this.dDateTime.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dDateTime.Location = new System.Drawing.Point(119, 80);
            this.dDateTime.Name = "dDateTime";
            this.dDateTime.Size = new System.Drawing.Size(206, 20);
            this.dDateTime.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Fecha y Hora:";
            // 
            // tRiskArea
            // 
            this.tRiskArea.Location = new System.Drawing.Point(119, 54);
            this.tRiskArea.Name = "tRiskArea";
            this.tRiskArea.Size = new System.Drawing.Size(206, 20);
            this.tRiskArea.TabIndex = 13;
            this.tRiskArea.Enter += new System.EventHandler(this.tRiskArea_Enter);
            // 
            // tId
            // 
            this.tId.Location = new System.Drawing.Point(119, 28);
            this.tId.Name = "tId";
            this.tId.Size = new System.Drawing.Size(206, 20);
            this.tId.TabIndex = 12;
            this.tId.Enter += new System.EventHandler(this.tId_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Área de Riesgo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Id:";
            // 
            // UIAccident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.groupBox1);
            this.Name = "UIAccident";
            this.Size = new System.Drawing.Size(399, 385);
            this.Load += new System.EventHandler(this.UIAccident_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablePersonalImpact)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nInvolved)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nInvolved;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dDateTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tRiskArea;
        private System.Windows.Forms.TextBox tId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lCount;
        private System.Windows.Forms.ComboBox cbImpact;
        private System.Windows.Forms.Button bDeleteRiskFactor;
        private System.Windows.Forms.DataGridView tablePersonalImpact;
        private System.Windows.Forms.Button bAddRiskFactor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bSave;
    }
}
