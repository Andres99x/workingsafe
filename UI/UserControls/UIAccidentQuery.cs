﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Domain.Models;
using GMap.NET;


using GMap.NET.WindowsForms;namespace UI.UserControls
{
    public partial class UIAccidentQuery : UserControl
    {
        public UIAccidentQuery()
        {
            InitializeComponent();
            List<RiskArea> ra = Domain.System.Instance().GetRiskAreas();
            List<string> ds = new List<string>();

            foreach(RiskArea r in ra)
            {
                ds.Add(r.Id);
            }

            this.RiskAreaCombo.DataSource = ds; 
        }

        private void QueryButton_Click(object sender, EventArgs e)
        {
            string id = (string)this.RiskAreaCombo.SelectedValue;

            if (id == null)
                return;

            List<Accident> accidents = Domain.System.Instance().GetAccidentsInRiskArea(id);

            DataTable dt = new DataTable();
            string[] header = new string[] { "Identificador", "Área de Riesgo", "Fecha", "Número de Involucrados" };

            foreach (string h in header)
            {
                dt.Columns.Add(h);
            }

            foreach (Accident a in accidents)
            {
                DataRow row = dt.NewRow();
                row.ItemArray = new string[] { a.Id, a.RiskArea.Id,a.Date.ToString(),a.Impacts.Count.ToString()};
                dt.Rows.Add(row);
            }

            AccidentsTable.DataSource = null;
            AccidentsTable.DataSource = dt;
        }



    }
}
