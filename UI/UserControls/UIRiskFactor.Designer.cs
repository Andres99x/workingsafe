﻿namespace UI.UserControls
{
    partial class UIRiskFactor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bCancel = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.lName = new System.Windows.Forms.Label();
            this.lId = new System.Windows.Forms.Label();
            this.lDescription = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(173, 103);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 15;
            this.bCancel.Text = "Cancelar";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // bAdd
            // 
            this.bAdd.Location = new System.Drawing.Point(84, 103);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(75, 23);
            this.bAdd.TabIndex = 14;
            this.bAdd.Text = "Agregar";
            this.bAdd.UseVisualStyleBackColor = true;
            // 
            // lName
            // 
            this.lName.Location = new System.Drawing.Point(81, 32);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(167, 13);
            this.lName.TabIndex = 13;
            this.lName.Text = "Nombre";
            // 
            // lId
            // 
            this.lId.Location = new System.Drawing.Point(81, 9);
            this.lId.Name = "lId";
            this.lId.Size = new System.Drawing.Size(167, 13);
            this.lId.TabIndex = 12;
            this.lId.Text = "Identificador";
            // 
            // lDescription
            // 
            this.lDescription.Location = new System.Drawing.Point(81, 55);
            this.lDescription.Name = "lDescription";
            this.lDescription.Size = new System.Drawing.Size(167, 45);
            this.lDescription.TabIndex = 11;
            this.lDescription.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Descripción:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Id:";
            // 
            // UIRiskFactor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.lName);
            this.Controls.Add(this.lId);
            this.Controls.Add(this.lDescription);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UIRiskFactor";
            this.Size = new System.Drawing.Size(259, 136);
            this.Load += new System.EventHandler(this.UIRiskFactor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button bCancel;
        public System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Label lId;
        private System.Windows.Forms.Label lDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;

    }
}
