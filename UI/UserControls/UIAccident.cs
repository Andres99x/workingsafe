﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using UI.Forms;
using System.Windows.Forms;
using Domain.Models;
using Domain;

namespace UI.UserControls
{
    public partial class UIAccident : UserControl
    {
        private DataTable table;
        private UIWindow win;

        public UIAccident(UIWindow win)
        {
            InitializeComponent();
            this.table = new DataTable();
            this.win = win;
        }

        private void UIAccident_Load(object sender, EventArgs e)
        {
            this.table.Columns.Add(new DataColumn("Impacto Personal", typeof(string)));
            this.tablePersonalImpact.DataSource = null;
            this.tablePersonalImpact.DataSource = this.table;
        }
        
        private void bAddRiskFactor_Click(object sender, EventArgs e)
        {
            if (this.nInvolved.Value > this.tablePersonalImpact.Rows.Count)
            {
                if (cbImpact.SelectedItem != null && (string)cbImpact.SelectedItem != "")
                {
                    DataRow row = this.table.NewRow();
                    row[0] = (string)cbImpact.SelectedItem;
                    this.table.Rows.Add(row);
                    this.tablePersonalImpact.DataSource = null;
                    this.tablePersonalImpact.DataSource = this.table;
                    this.lCount.Text = this.tablePersonalImpact.Rows.Count + "/" + this.nInvolved.Value;
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un tipo de Impacto Personal.", "Error - Impacto Personal", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Ya ha alcanzado el N° de Involucrados.", "Error - Impacto Personal", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bDeleteRiskFactor_Click(object sender, EventArgs e)
        {
            if (this.tablePersonalImpact.SelectedRows.Count > 0)
            {
                this.table.Rows.RemoveAt(this.tablePersonalImpact.SelectedRows[0].Index);
                this.tablePersonalImpact.DataSource = null;
                this.tablePersonalImpact.DataSource = this.table;
                this.lCount.Text = this.tablePersonalImpact.Rows.Count + "/" + this.nInvolved.Value;
            }
        }
        
        private void nInvolved_ValueChanged(object sender, EventArgs e)
        {
            this.lCount.Text = this.tablePersonalImpact.Rows.Count + "/" + this.nInvolved.Value;
        }

        private void loadAutocomplete()
        {
            this.tRiskArea.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.tRiskArea.AutoCompleteSource = AutoCompleteSource.CustomSource;

            AutoCompleteStringCollection res = new AutoCompleteStringCollection();

            foreach (RiskArea ra in Domain.System.Instance().GetRiskAreas())
            {
                res.Add(ra.Id);
                res.Add(ra.Name);
            }
            this.tRiskArea.AutoCompleteCustomSource = res;
        }

        private bool isAlfaNum(string text)
        {
            foreach (char c in text)
            {
                if (!Char.IsLetterOrDigit(c) && c != '_') { return false; }
            }
            return true;
        }

        private bool isValidDate(DateTime dateTime)
        {
            return DateTime.Now > dateTime;
        }

        private string isRiskArea(string text)
        {
            if (text.Count() > 0)
            {
                foreach (string s in this.tRiskArea.AutoCompleteCustomSource)
                {
                    if (s.ToLower().Equals(text.ToLower()))
                    {
                        return s;
                    }
                }
            }
            return null;
        }

        private bool Valid()
        {
            string msj = "";
            if (!this.isAlfaNum(this.tId.Text) || this.tId.Text.Count() == 0)
            {
                msj += "Id no es Alfa/Numérico o está vacío.\n";
            }
            else
            {
                string suggested = Domain.System.Instance().AvailableAccidentId(this.tId.Text);
                if (suggested != this.tId.Text)
                {
                    msj += "Id ingresado ya existe, " + suggested + " sugerido.\n";
                    this.tId.Text = suggested;
                    this.tId.ForeColor = Color.Blue;
                }
            }
            if (this.isRiskArea(this.tRiskArea.Text) == null)
            {
                msj += "Debe indicar un Área de Riesgo sugerida.\n";
                this.tRiskArea.ForeColor = Color.Red;
            }
            if (!this.isValidDate(this.dDateTime.Value))
            {
                msj += "La fecha del accidente debe ser anterior a la actual.\n";
            }
            if (this.nInvolved.Value != this.tablePersonalImpact.Rows.Count)
            {
                msj += "Debe tener tantos Impactos Personales, como N° de Involucrados.\n";
            }
            if (msj != "")
            {
                MessageBox.Show(msj, "Error - Alta Accidente", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
             if(this.Valid())
             {
                 List<string> list = new List<string>();
                 foreach (DataRow row in this.table.Rows)
                 {
                     list.Add((string)row[0]);
                 }
                 try
                 {
                     if (Domain.System.Instance().NewAccident(this.tId.Text, this.isRiskArea(this.tRiskArea.Text), this.dDateTime.Value, list))
                     {
                         MessageBox.Show("Accidente " + this.tId.Text + " ha sido guardado.", "Guardado Exitoso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                         this.bCancel_Click(null, null);
                     }
                     else
                     {
                         MessageBox.Show("Accidente " + this.tId.Text + " no pudo ser guardado.", "Error Guardado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     }
                 }
                 catch (Exception ex)
                 {
                     MessageBox.Show(ex.Message, "Error Guardado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 }
             }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            this.loadAutocomplete();
        }

        private void tId_Enter(object sender, EventArgs e)
        {
            this.tId.ForeColor = Color.Black;
        }

        private void tRiskArea_Enter(object sender, EventArgs e)
        {
            this.tRiskArea.ForeColor = Color.Black;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.win.AccidentCancel();
        }               
    }
}
