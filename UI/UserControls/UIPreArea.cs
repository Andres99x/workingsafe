﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Domain.Models;

namespace UI.UserControls
{
    public partial class UIPreArea : UserControl
    {
        public UIPreArea()
        {
            InitializeComponent();
        }

        public bool Valid()
        {
            return (cbBuilding.SelectedItem != null && (string)cbBuilding.SelectedItem != "") || 
                (cbFloor.SelectedItem != null && (string)cbFloor.SelectedItem != "") || 
                (cbSector.SelectedItem != null && (string)cbSector.SelectedItem != "") || 
                (cbOrientation.SelectedItem != null && (string)cbOrientation.SelectedItem != "");
        }

        private void UIPreArea_Load(object sender, EventArgs e)
        {
            this.bSave.DialogResult = DialogResult.OK;
            this.bCancel.DialogResult = DialogResult.Cancel;
        }

        public PreDefMap GetPreDefMap()
        {
            return new PreDefMap((string)cbBuilding.SelectedItem, (string)cbFloor.SelectedItem, (string)cbSector.SelectedItem, (string)cbOrientation.SelectedItem);
        }

    }
}
