﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Domain.Models;

namespace UI.UserControls
{
    public partial class UIRiskFactor : UserControl
    {
        public UIRiskFactor(RiskFactor rf)
        {
            InitializeComponent();
            this.lId.Text = rf.Id;
            this.lName.Text = rf.Name;
            this.lDescription.Text = rf.Description;
        }

        private void UIRiskFactor_Load(object sender, EventArgs e)
        {
            this.bAdd.DialogResult = DialogResult.OK;
            this.bCancel.DialogResult = DialogResult.Cancel;
        }
    }
}
