﻿namespace UI.UserControls
{
    partial class UIGeoArea
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAddEntry = new System.Windows.Forms.CheckBox();
            this.bAddPoint = new System.Windows.Forms.CheckBox();
            this.gMap = new GMap.NET.WindowsForms.GMapControl();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bSearch);
            this.groupBox1.Controls.Add(this.tSearch);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.bCancel);
            this.groupBox1.Controls.Add(this.bSave);
            this.groupBox1.Controls.Add(this.bClear);
            this.groupBox1.Controls.Add(this.bAddEntry);
            this.groupBox1.Controls.Add(this.bAddPoint);
            this.groupBox1.Controls.Add(this.gMap);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(732, 492);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Área Georreferenciada";
            // 
            // bSearch
            // 
            this.bSearch.Location = new System.Drawing.Point(550, 19);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 9;
            this.bSearch.Text = "Buscar";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(55, 20);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(489, 20);
            this.tSearch.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Buscar:";
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(631, 463);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(94, 23);
            this.bCancel.TabIndex = 6;
            this.bCancel.Text = "Cancelar";
            this.bCancel.UseVisualStyleBackColor = true;
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(631, 434);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(94, 23);
            this.bSave.TabIndex = 5;
            this.bSave.Text = "Guardar";
            this.bSave.UseVisualStyleBackColor = true;
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(631, 106);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(94, 23);
            this.bClear.TabIndex = 4;
            this.bClear.Text = "Limpiar";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAddEntry
            // 
            this.bAddEntry.Appearance = System.Windows.Forms.Appearance.Button;
            this.bAddEntry.AutoSize = true;
            this.bAddEntry.Location = new System.Drawing.Point(631, 77);
            this.bAddEntry.Name = "bAddEntry";
            this.bAddEntry.Size = new System.Drawing.Size(94, 23);
            this.bAddEntry.TabIndex = 3;
            this.bAddEntry.Text = "Agregar Entrada";
            this.bAddEntry.UseVisualStyleBackColor = true;
            this.bAddEntry.CheckedChanged += new System.EventHandler(this.bAddEntry_CheckedChanged);
            // 
            // bAddPoint
            // 
            this.bAddPoint.Appearance = System.Windows.Forms.Appearance.Button;
            this.bAddPoint.Location = new System.Drawing.Point(631, 48);
            this.bAddPoint.Name = "bAddPoint";
            this.bAddPoint.Size = new System.Drawing.Size(94, 23);
            this.bAddPoint.TabIndex = 2;
            this.bAddPoint.Text = "Agregar Punto";
            this.bAddPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bAddPoint.UseVisualStyleBackColor = true;
            this.bAddPoint.CheckedChanged += new System.EventHandler(this.bAddPoint_CheckedChanged);
            // 
            // gMap
            // 
            this.gMap.Bearing = 0F;
            this.gMap.CanDragMap = true;
            this.gMap.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMap.GrayScaleMode = false;
            this.gMap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMap.LevelsKeepInMemmory = 5;
            this.gMap.Location = new System.Drawing.Point(6, 48);
            this.gMap.MarkersEnabled = true;
            this.gMap.MaxZoom = 18;
            this.gMap.MinZoom = 0;
            this.gMap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionWithoutCenter;
            this.gMap.Name = "gMap";
            this.gMap.NegativeMode = false;
            this.gMap.PolygonsEnabled = true;
            this.gMap.RetryLoadTile = 0;
            this.gMap.RoutesEnabled = true;
            this.gMap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMap.ShowTileGridLines = false;
            this.gMap.Size = new System.Drawing.Size(619, 438);
            this.gMap.TabIndex = 0;
            this.gMap.Zoom = 13D;
            this.gMap.DoubleClick += new System.EventHandler(this.gMap_DoubleClick);
            // 
            // UIGeoArea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.groupBox1);
            this.Name = "UIGeoArea";
            this.Size = new System.Drawing.Size(735, 495);
            this.Load += new System.EventHandler(this.UIGeoArea_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.CheckBox bAddEntry;
        private System.Windows.Forms.CheckBox bAddPoint;
        private GMap.NET.WindowsForms.GMapControl gMap;
        public System.Windows.Forms.Button bCancel;
        public System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Label label1;

        
    }
}
