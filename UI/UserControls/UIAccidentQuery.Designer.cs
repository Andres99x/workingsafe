﻿namespace UI.UserControls
{
    partial class UIAccidentQuery
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RiskAreaCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.QueryButton = new System.Windows.Forms.Button();
            this.AccidentsTable = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // RiskAreaCombo
            // 
            this.RiskAreaCombo.FormattingEnabled = true;
            this.RiskAreaCombo.Location = new System.Drawing.Point(106, 16);
            this.RiskAreaCombo.Name = "RiskAreaCombo";
            this.RiskAreaCombo.Size = new System.Drawing.Size(261, 21);
            this.RiskAreaCombo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Área de Riesgo:";
            // 
            // QueryButton
            // 
            this.QueryButton.Location = new System.Drawing.Point(373, 16);
            this.QueryButton.Name = "QueryButton";
            this.QueryButton.Size = new System.Drawing.Size(75, 23);
            this.QueryButton.TabIndex = 3;
            this.QueryButton.Text = "Consultar";
            this.QueryButton.UseVisualStyleBackColor = true;
            this.QueryButton.Click += new System.EventHandler(this.QueryButton_Click);
            // 
            // AccidentsTable
            // 
            this.AccidentsTable.AllowUserToAddRows = false;
            this.AccidentsTable.AllowUserToDeleteRows = false;
            this.AccidentsTable.AllowUserToResizeRows = false;
            this.AccidentsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.AccidentsTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AccidentsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AccidentsTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.AccidentsTable.Location = new System.Drawing.Point(20, 46);
            this.AccidentsTable.MultiSelect = false;
            this.AccidentsTable.Name = "AccidentsTable";
            this.AccidentsTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.AccidentsTable.RowHeadersVisible = false;
            this.AccidentsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AccidentsTable.ShowCellErrors = false;
            this.AccidentsTable.ShowCellToolTips = false;
            this.AccidentsTable.ShowEditingIcon = false;
            this.AccidentsTable.ShowRowErrors = false;
            this.AccidentsTable.Size = new System.Drawing.Size(428, 171);
            this.AccidentsTable.TabIndex = 8;
            // 
            // UIAccidentQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.AccidentsTable);
            this.Controls.Add(this.QueryButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RiskAreaCombo);
            this.Name = "UIAccidentQuery";
            this.Size = new System.Drawing.Size(468, 237);
            ((System.ComponentModel.ISupportInitialize)(this.AccidentsTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox RiskAreaCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button QueryButton;
        private System.Windows.Forms.DataGridView AccidentsTable;
    }
}
