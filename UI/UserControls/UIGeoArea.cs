﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Domain.Models;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;

namespace UI.UserControls
{
    public partial class UIGeoArea : UserControl
    {
        private List<PointLatLng> PolygonPoints;
        private List<PointLatLng> Entrys;

        public UIGeoArea()
        {
            InitializeComponent();
            this.PolygonPoints = new List<PointLatLng>();
            this.Entrys = new List<PointLatLng>();
        }

        private void bAddPoint_CheckedChanged(object sender, EventArgs e)
        {
            if (this.bAddPoint.Checked)
            {
                this.bAddEntry.Checked = false;
            }
        }

        private void bAddEntry_CheckedChanged(object sender, EventArgs e)
        {
            if (this.bAddEntry.Checked)
            {
                this.bAddPoint.Checked = false;
            }
        }

        public bool Valid()
        {
            if(this.PolygonPoints.Count > 2)
            {
                return true;
            }
            return false;
        }

        public GeoMap GetGeoMap()
        {
            GeoMap map = new GeoMap();
            foreach(PointLatLng p in this.PolygonPoints)
            {
                map.AreaPoints.Add(new Domain.Models.Point(p.Lat, p.Lng));
            }
            foreach (PointLatLng p in this.Entrys)
            {
                map.EntrancePoints.Add(new Domain.Models.Point(p.Lat, p.Lng));
            }
            return map;
        }

        private void UIGeoArea_Load(object sender, EventArgs e)
        {
            this.bSave.DialogResult = DialogResult.OK;
            this.bCancel.DialogResult = DialogResult.Cancel;

            this.gMap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
            this.gMap.SetCurrentPositionByKeywords("Montevideo, Uruguay");
            this.gMap.Overlays.Add(new GMapOverlay("area"));
            this.gMap.Overlays.FirstOrDefault(o => o.Id == "area").Polygons.Add(new GMapPolygon(new List<PointLatLng>(),"polygon"));
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            this.gMap.SetCurrentPositionByKeywords(this.tSearch.Text);   
        }
        
        private void gMap_DoubleClick(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if(this.bAddEntry.Checked)
            {
                this.Entrys.Add(this.gMap.FromLocalToLatLng(me.X, me.Y));
                this.gMap.Overlays.FirstOrDefault(o => o.Id == "area").Markers.Add(new GMarkerGoogle(this.Entrys.Last(),GMarkerGoogleType.blue));        
            }
            else if(this.bAddPoint.Checked)
            {
                this.PolygonPoints.Add(this.gMap.FromLocalToLatLng(me.X, me.Y));
                this.gMap.Overlays.FirstOrDefault(o => o.Id == "area").Polygons.Clear();
                this.gMap.Overlays.FirstOrDefault(o => o.Id == "area").Polygons.Add(new GMapPolygon(this.PolygonPoints,"polygon"));
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            this.gMap.Overlays.FirstOrDefault(o => o.Id == "area").Markers.Clear();
            this.gMap.Overlays.FirstOrDefault(o => o.Id == "area").Polygons.Clear();
            this.PolygonPoints.Clear();
            this.Entrys.Clear();
        }
    }
}
