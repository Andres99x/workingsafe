﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UI.UserControls;
using UI.Forms;
using Domain;
using Domain.Models;

namespace UI.UserControls
{
    public partial class UIRiskArea : UserControl
    {
        private UIWindow win;

        public UIRiskArea(UIWindow win)
        {
            InitializeComponent();
            this.win = win;
        }

        private UIGeoArea GeoArea;
        private UIPreArea PreArea;

        private DataTable table;
        
        private void UIRiskArea_Load(object sender, EventArgs e)
        {
            this.table = new DataTable();
            table.Columns.Add("Id",typeof(string));
            table.Columns[0].ReadOnly = true;
            table.Columns.Add("Nombre", typeof(string));
            table.Columns[0].ReadOnly = true;
            table.Columns.Add("Monitor",typeof(bool));
            table.Columns[0].ReadOnly = false;
            this.tableRiskFactors.DataSource = table;
            this.loadAutocomplete();
        }

        private void bGeo_Click(object sender, EventArgs e)
        {
            if (this.PreArea == null)
            {
                UIInputDialogWindow form = new UIInputDialogWindow();
                if(this.GeoArea == null)
                {
                    this.GeoArea = new UIGeoArea();
                }
                form.Text = "Área Georreferenciada";
                form.Controls.Add(this.GeoArea);
                form.AcceptButton = this.GeoArea.bSave;
                form.CancelButton = this.GeoArea.bCancel;

                bool ok = false;
                do
                {
                    if (DialogResult.OK == form.ShowDialog())
                    {
                        if (this.GeoArea.Valid())
                        {
                            ok = true;
                            this.bGeo.BackColor = Color.YellowGreen;
                        }
                        else
                        {
                            MessageBox.Show(this, "El Área Georreferenciada determinada no es válida.\nDebe tener almenos 3 ptos no alineados\ny un punto de entrada.", "Error - Georreferenciada", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        ok = true;
                        this.GeoArea = null;
                    }
                } while (!ok);
            }
            else
            {
                if (DialogResult.Yes == MessageBox.Show(this,"Ya tiene un Área Predefinida determinada,\n¿desea eliminarla y continuar?","Área ya definida",MessageBoxButtons.YesNo,MessageBoxIcon.Warning))
                {
                    this.PreArea = null;
                    this.bPre.BackColor = SystemColors.Control;
                    this.bGeo_Click(null, null);
                }
            }
        }

        private void bPre_Click(object sender, EventArgs e)
        {
            if(this.GeoArea == null)
            {            
                UIInputDialogWindow form = new UIInputDialogWindow();
                if(this.PreArea == null){
                    this.PreArea = new UIPreArea();
                }

                form.Text = "Área Predefinida";
                form.Controls.Add(this.PreArea);
                form.AcceptButton = this.PreArea.bSave;
                form.CancelButton = this.PreArea.bCancel;
                
                bool ok = false;
                do{ 
                    if (DialogResult.OK == form.ShowDialog())
                    {
                        if (this.PreArea.Valid())
                        {
                            ok = true;
                            this.bPre.BackColor = Color.YellowGreen;
                        }
                        else
                        {
                            MessageBox.Show("El Área Predefinida determinada no es válida.","Error - Área Predefinida",MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        ok = true;
                        this.PreArea = null;
                    }
                }while(!ok);
            }
            else
            {
                if (DialogResult.Yes == MessageBox.Show("Ya tiene un Área Georreferenciada determinada,\n¿desea eliminarla y continuar?","Área ya definida",MessageBoxButtons.YesNo,MessageBoxIcon.Warning))
                {
                    this.GeoArea = null;
                    this.bGeo.BackColor = SystemColors.Control;
                    this.bPre_Click(null, null);
                }
            }
        }

        private void bAddRiskFactor_Click(object sender, EventArgs e)
        {            
            string autocompleteValue;
            if ((autocompleteValue = this.isRiskFactor(this.tRiskFactor.Text)) != null && !this.isRiskFactorAdded(autocompleteValue))
            {
                RiskFactor aux = Domain.System.Instance().GetRiskFactor(autocompleteValue);
                if (aux != null)
                {
                    UIInputDialogWindow form = new UIInputDialogWindow();
                    UIRiskFactor rf = new UIRiskFactor(aux);

                    form.Text = "Factor de Riesgo";
                    form.Controls.Add(rf);
                    form.AcceptButton = rf.bAdd;
                    form.CancelButton = rf.bCancel;

                    if (DialogResult.OK == form.ShowDialog())
                    {
                        DataRow row = this.table.NewRow();
                        row[0] = aux.Id;
                        row[1] = aux.Name;
                        row[2] = false;
                        this.table.Rows.Add(row);
                        this.tableRiskFactors.DataSource = null;
                        this.tableRiskFactors.DataSource = this.table;
                        this.tRiskFactor.Text = "";
                    }
                }
                else
                {
                    MessageBox.Show("No se encontro el Factor de Riesgo indicado, intente de nuevo.", "Error - Factor de Riesgo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Factor de Riesgo no existe, seleccione uno de los sugeridos.");
            }
        }

        private void bDeleteRiskFactor_Click(object sender, EventArgs e)
        {
            
            if(this.tableRiskFactors.SelectedRows.Count > 0 )
            {
                this.table.Rows.RemoveAt(this.tableRiskFactors.SelectedRows[0].Index);
                this.tableRiskFactors.DataSource = null;
                this.tableRiskFactors.DataSource = this.table;
            }
        }
        
        private void tId_TextChanged(object sender, EventArgs e)
        {
            this.tId.ForeColor = Color.Black;
        }

        private void tName_TextChanged(object sender, EventArgs e)
        {
            this.tName.ForeColor = Color.Black;
        }
        
        private void loadAutocomplete()
        {
            this.tRiskFactor.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            this.tRiskFactor.AutoCompleteSource = AutoCompleteSource.CustomSource;

            AutoCompleteStringCollection res = new AutoCompleteStringCollection();

            foreach (RiskFactor rf in Domain.System.Instance().GetRiskFactors())
            {
                res.Add(rf.Id);
                res.Add(rf.Name);
            }
            this.tRiskFactor.AutoCompleteCustomSource = res;
        }

        private bool isAlfaNum(string text)
        {
            foreach (char c in text)
            {
                if (!Char.IsLetterOrDigit(c) && c != '_') { return false; }
            }
            return true;
        }

        private string isRiskFactor(string text)
        {
            if(text.Count() > 0)
            {
                foreach(string s in this.tRiskFactor.AutoCompleteCustomSource)
                {
                    if(s.ToLower().Equals(text.ToLower()))
                    {
                        return s;
                    }
                }
            }
            return null;
        }

        private bool isRiskFactorAdded(string idName)
        {
            foreach(DataRow row in this.table.Rows)
            {
                if (row[0].ToString() == idName || row[1].ToString() == idName)
                {
                    return true;
                }
            }
            return false;
        }

        private bool isValid()
        {
            string msj = "";
            if (!this.isAlfaNum(this.tId.Text) || this.tId.Text.Count() == 0)
            {
                this.tId.ForeColor = Color.Red;
                msj += "Id no es Alfa/Numérico o está vacío.\n";
            }
            else
            {
                string suggested = Domain.System.Instance().AvailableRiskAreaId(this.tId.Text);
                if (suggested != this.tId.Text)
                {
                    msj += "Id ingresado ya existe, " + suggested + " sugerido.\n";
                    this.tId.Text = suggested;
                    this.tId.ForeColor = Color.Blue;
                }
            }
            if (!this.isAlfaNum(this.tName.Text) || this.tName.Text.Count() == 0)
            {
                this.tName.ForeColor = Color.Red;
                msj += "Nombre no es Alfa/Numéricoo está vacío.\n";
            }
            else
            {
                string suggested = Domain.System.Instance().AvailableRiskAreaName(this.tName.Text);
                if (suggested != this.tName.Text)
                {
                    msj += "Nombre ingresado ya existe, " + suggested + " sugerido.\n";
                    this.tName.Text = suggested;
                    this.tName.ForeColor = Color.Blue;
                }
            }
            if(this.nLevel.Value == 0)
            {
                msj += "Debe ingresar un nivel de Capacitacion mayor a 0.\n";
            }
            if (this.GeoArea == null && this.PreArea == null)
            {
                msj += "No determino ningún tipo de área.";
            }
            if (msj.Count() > 0)
            {
                MessageBox.Show(msj, "Error - Datos Inválidos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            if(this.isValid())
            {
                List<Tuple<string,bool>> monitor = new List<Tuple<string,bool>>();
                foreach(DataRow row in this.table.Rows)
                {
                    monitor.Add(new Tuple<string,bool>((string)row[0],(bool)row[2]));
                }
                try
                {
                    if (Domain.System.Instance().NewRiskArea(this.tId.Text, this.tName.Text, monitor, (int)this.nLevel.Value,
                                        this.GeoArea == null ? null : this.GeoArea.GetGeoMap(),
                                        this.PreArea == null ? null : this.PreArea.GetPreDefMap()))
                    {
                        MessageBox.Show("Area de Riesgo " + this.tId.Text + " ha sido guardada.", "Guardado Exitoso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        this.bCancel_Click(null, null);
                    }
                    else
                    {
                        MessageBox.Show("Area de Riesgo " + this.tId.Text + " no pudo ser guardada.", "Error Guardado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error Guardado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            this.loadAutocomplete();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.win.RiskAreaCancel();
        }
    }
}
