﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Point
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public double Latitude { get; set; }
        
        [Required]
        public double Longitude { get; set; }

        public Point() { }

        public Point(double lat, double lon)
        {
            this.Latitude = lat;
            this.Longitude = lon;
        }
    }
}
