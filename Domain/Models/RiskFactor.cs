﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class RiskFactor
    {
        [Key]
        public string Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public RiskFactor() { }

        public RiskFactor(string id, string name, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
        }
    }
}
