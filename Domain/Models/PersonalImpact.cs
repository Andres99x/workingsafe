﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class PersonalImpact
    {
        [Key]
        public int Id { get; set; }

        public string Impact { get; set; }

        public PersonalImpact() { }
        public PersonalImpact(string i)
        {
            this.Impact = i;
        }
    }
}
