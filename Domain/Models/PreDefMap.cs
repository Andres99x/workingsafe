﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class PreDefMap
    {
        [Key]
        public int Id { get; set; }

        public string Building { get; set; }
        public string Floor { get; set; }
        public string Sector { get; set; }
        public string Orientation { get; set; }

        public PreDefMap() { }

        public PreDefMap(string building, string floor, string sector, string orientation)
        {
            this.Building = building;
            this.Floor = floor;
            this.Sector = sector;
            this.Orientation = orientation;
        }
    }
}
