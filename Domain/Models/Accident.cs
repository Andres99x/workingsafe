﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Accident
    {
        [Key]
        public string Id { get; set; }

        public virtual RiskArea RiskArea { get; set; }
        public DateTime Date { get; set; }
        public virtual List<PersonalImpact> Impacts { get; set; }

        public Accident()
        {
            Impacts = new List<PersonalImpact>();
        }

        public Accident(string id, RiskArea ra, DateTime date)
        {
            this.Id = id;
            this.RiskArea = ra;
            this.Date = date;
            this.Impacts = new List<PersonalImpact>();
        }
    }
}
