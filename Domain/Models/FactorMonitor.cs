﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class FactorMonitor
    {
        [Key]
        public int Id { get; set; }

        public virtual RiskFactor factor { get; set; }
        public bool HasMonitor { get; set; }

        public FactorMonitor()
        { }
        public FactorMonitor(RiskFactor f, bool hs)
        {
            this.factor = f;
            this.HasMonitor = hs;
        }
    }
}
