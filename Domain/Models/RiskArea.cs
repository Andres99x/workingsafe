﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    
    public class RiskArea
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }

        public virtual List<FactorMonitor> FactorsData { get; set; }

        public int CapacitationLevel { get; set; }

        public virtual GeoMap GeoMap { get; set; }
        public virtual PreDefMap PreDefMap { get; set; }

        public RiskArea()
        {
            FactorsData = new List<FactorMonitor>();
        }

        public RiskArea(string id, string name, int capLevel, GeoMap map)
        {
            this.Id = id;
            this.Name = name;
            this.CapacitationLevel = capLevel;
            this.GeoMap = map;
        }

        public RiskArea(string id, string name, int capLevel, PreDefMap map)
        {
            this.Id = id;
            this.Name = name;
            this.CapacitationLevel = capLevel;
            this.PreDefMap = map;
        }
    }
}
