﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace Domain.Models
{
    public class GeoMap
    {
        [Key]
        public int Id { get; set; }

        public virtual List<Point> EntrancePoints { get; set; }
        public virtual List<Point> AreaPoints { get; set; }

        public GeoMap()
        {
            this.EntrancePoints = new List<Point>();
            this.AreaPoints = new List<Point>();
        }
    }
}
