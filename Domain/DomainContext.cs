﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Domain.Models;

namespace Domain
{
    public class DomainContext:DbContext
    {
        public DbSet<RiskFactor> RiskFactors { get; set; }
        public DbSet<RiskArea> RiskAreas { get; set; }
        public DbSet<FactorMonitor> FactorMonitors { get; set; }
        public DbSet<GeoMap> GeoMaps { get; set; }
        public DbSet<PreDefMap> PreDefMaps { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<Accident> Accidents { get; set; }
        public DbSet<PersonalImpact> Impacts { get; set; }
        
    }
}
