﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models;

namespace Domain
{
    public class DBManager
    {
        private static DBManager _instance { get; set; }
        public DomainContext Context { get; set; }
        
        private DBManager()
        {
            Context = new DomainContext();
        }

        public static DBManager Instance()
        {
            if (_instance == null)
            {
                _instance = new DBManager();
            }
            return _instance;
        }

        public RiskArea GetRiskAreaById(string id)
        {
            return Context.RiskAreas.Where(ra => ra.Id == id).FirstOrDefault();
        }

        public RiskFactor GetRiskFactorByIdOrName(string idName)
        {
            return Context.RiskFactors.Where(rf => rf.Id == idName || rf.Name == idName).FirstOrDefault();
        }

        public RiskFactor GetRiskFactorById(string id)
        {
            return Context.RiskFactors.Where(rf => rf.Id == id).FirstOrDefault();
        }

        public RiskArea getRiskArea(string idNombre)
        {
            return Context.RiskAreas.Where(rf => rf.Id == idNombre || rf.Name == idNombre).FirstOrDefault();
        }

        public bool RiskAreaIdExists(string id)
        {
            return Context.RiskAreas.Where(ra => ra.Id == id).FirstOrDefault() != null;
        }

        public bool RiskAreaNameExists(string name)
        {
            return Context.RiskAreas.Where(ra => ra.Name == name).FirstOrDefault() != null;
        }

        public bool RiskFactorExists(string id)
        {
            return Context.RiskFactors.Where(rf => rf.Id == id).FirstOrDefault() != null;
        }

        public bool AccidentIdExists(string id)
        {
            return Context.Accidents.Where(a => a.Id == id).FirstOrDefault() != null;
        }

        public List<RiskFactor> GetRiskFactors()
        {
            return Context.RiskFactors.ToList<RiskFactor>();
        }

        public List<Accident> GetAccidents()
        {
            return Context.Accidents.ToList();
        }

        public List<Accident> GetAccidentsInRiskArea(string riskAreaId)
        {
            return Context.Accidents.Where(a => a.RiskArea.Id == riskAreaId).ToList();
        }
    }
}
