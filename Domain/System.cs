﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Domain.Models;

namespace Domain
{
    public class System
    {
        private static System _instance { get; set; }

        private System() 
        {
            this.Initialize();
        }

        public static System Instance()
        {
            if (_instance == null)
                _instance = new System();

            return _instance;
        }

        public List<RiskFactor> GetRiskFactors() 
        {
            return DBManager.Instance().Context.RiskFactors.ToList();
        }

        public RiskFactor GetRiskFactor(string idNombre)
        {
            return DBManager.Instance().GetRiskFactorByIdOrName(idNombre);
        }

        public List<RiskArea> GetRiskAreas()
        {
            return DBManager.Instance().Context.RiskAreas.ToList();
        }

        public string AvailableRiskAreaId(string id)
        {
            if(DBManager.Instance().RiskAreaIdExists(id))
            {
                int cont = 0;
                string res = "AR_" + cont.ToString("00");
                while (DBManager.Instance().RiskAreaIdExists(res))
                {
                    cont++;
                    res = "AR_" + cont.ToString("00");
                }
                return res;
            }
            return id;
        }

        public string AvailableRiskAreaName(string name)
        {
            if (DBManager.Instance().RiskAreaNameExists(name))
            {
                int cont = 0;
                string res = name + cont.ToString("00");
                while (DBManager.Instance().RiskAreaNameExists(res))
                {
                    cont++;
                    res = name + cont.ToString("00");
                }
                return res;
            }
            return name;
        }

        public string AvailableAccidentId(string id)
        {
            if (DBManager.Instance().AccidentIdExists(id))
            {
                int cont = 0;
                string res = "ACC_" + cont.ToString("00");
                while (DBManager.Instance().AccidentIdExists(res))
                {
                    cont++;
                    res = "ACC_" + cont.ToString("00");
                }
                return res;
            }
            return id;
        }

        public bool NewRiskArea(string id, string name, List<Tuple<string,bool>> fm, int capLevel, GeoMap gm, PreDefMap pdm)
        {
            List<FactorMonitor> auxFm = new List<FactorMonitor>();
            foreach(Tuple<string,bool> t in fm)
            {
                auxFm.Add(new FactorMonitor(DBManager.Instance().GetRiskFactorByIdOrName(t.Item1), t.Item2));
            }

            if (!DBManager.Instance().RiskAreaIdExists(id))
            {
                RiskArea ra = gm != null ? new RiskArea(id, name, capLevel, gm) : new RiskArea(id, name, capLevel, pdm);
                ra.FactorsData = auxFm;
                try
                {
                    DBManager.Instance().Context.RiskAreas.Add(ra);
                    DBManager.Instance().Context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    throw new Exception("Hubo un error intentando guardar en la base de datos");
                }
            }

            throw new Exception("Ya existe un area de riesgo con ese identificador.");
        }
        
        public bool NewAccident(string id, string riskAreaIdName, DateTime date, List<string> impacts) 
        {
            if (!DBManager.Instance().AccidentIdExists(id))
            {
                Accident a = new Accident(id, DBManager.Instance().getRiskArea(riskAreaIdName), date);

                foreach (string i in impacts)
                { 
                    a.Impacts.Add(new PersonalImpact(i));
                }
                try
                {
                    DBManager.Instance().Context.Accidents.Add(a);
                    DBManager.Instance().Context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    throw new Exception("Hubo un error intentando guardar en la base de datos");
                }
            }

            throw new Exception("Ya existe un accidente con ese identificador.");
        }
        
        public void Initialize()
        { 
             //Add RiskFactor's to DB
            try
            {
                if (DBManager.Instance().Context.RiskFactors.Count() == 0)
                {
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_01", "Factor 1", "Este es el Factor de Riesgo 1"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_02", "Factor 2", "Este es el Factor de Riesgo 2"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_03", "Factor 3", "Este es el Factor de Riesgo 3"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_04", "Factor 4", "Este es el Factor de Riesgo 4"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_05", "Factor 5", "Este es el Factor de Riesgo 5"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_06", "Factor 6", "Este es el Factor de Riesgo 6"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_07", "Factor 7", "Este es el Factor de Riesgo 7"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_08", "Factor 8", "Este es el Factor de Riesgo 8"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_09", "Factor 9", "Este es el Factor de Riesgo 9"));
                    DBManager.Instance().Context.RiskFactors.Add(new RiskFactor("FR_10", "Factor 10", "Este es el Factor de Riesgo 10"));

                    DBManager.Instance().Context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<Accident> GetAccidentsInRiskArea(string riskAreaId)
        {
            return DBManager.Instance().GetAccidentsInRiskArea(riskAreaId);
        }

    }
}
